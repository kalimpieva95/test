import { Button } from "@mui/material";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import "./Modal.css";

export const CardModal = ({ country, onClose }) => {
  const [input, setInput] = useState({});
  const [commentList, setCommentList] = useState([]);

  if (!country) return null;

  function createComment(event) {
    const { name, value } = event.target;
    setInput({ ...input, [name]: value });
  }

  function commentCreation(input) {
    setCommentList([...commentList, input]);
    setInput((prev) => ({ ...prev, comment: "" }));
  }

  return (
    <Modal
      open={!!country}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="box">
        <h2>{country}</h2>
        <p></p>
        <TextField
          name="comment"
          type="text"
          placeholder="Your comment here?"
          required
          onChange={createComment}
        ></TextField>
        <Button
          onClick={commentCreation}
          type="submit"
          variant="contained"
          color="primary"
          className="buttonInput"
        >
          Добавить
        </Button>
      </Box>
    </Modal>
  );
};
