import axios from "axios";
import React, { useEffect, useState } from "react";
import Statistic from "../Statistic/Statistic";

export default function StatisticPage() {
  const [detailData, setDetailData] = useState();
  const [vaccinesData, setVaccinesData] = useState();
  // const [historyData, setHistoryData] = useState();
  const [data, setData] = useState([]);
  const [directionOfSorting, setDirectionOfSorting] = useState(true);
  const [filterColumn, setFilterColumn] = useState("");
  const [fData, setFdata] = useState();

  useEffect(() => {
    if (!(detailData && vaccinesData)) return;
    const countries = Object.keys(detailData.data);

    const hash = {};

    countries.forEach((c) => {
      if (vaccinesData.data[c]) {
        hash[c] = { ...detailData.data[c].All, ...vaccinesData.data[c].All };
      }
    });
    setData(Object.values(hash));
  }, [detailData, vaccinesData]);

  const sortInfo = (column) => {
    let sortedData;
    if (directionOfSorting) {
      sortedData = data.sort((a, b) => {
        return a[column] < b[column] ? -1 : 1;
      });
    }
    sortedData = data.reverse((a, b) => {
      return a[column] < b[column] ? -1 : 1;
    });

    setData([...sortedData]);
    setDirectionOfSorting(!directionOfSorting);
  };

  const handleChange = (event) => {
    setFilterColumn(event.target.value);
  };

  const filterInfo = (event) => {
    const { value } = event.target;

    const filterData = data.filter((el) => {
      if (el[filterColumn]) {
        return el[filterColumn].toLowerCase().includes(value.toLowerCase());
      }
    });

    setFdata([...filterData]);
  };

  useEffect(() => {
    axios
      .get("https://covid-api.mmediagroup.fr/v1/cases", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((data) => setDetailData(data));
    axios
      .get("https://covid-api.mmediagroup.fr/v1/vaccines", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((info) => setVaccinesData(info));
    // axios
    //   .get("https://covid-api.mmediagroup.fr/v1/history", {
    //     method: "GET",
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //   })
    //   .then((material) => setHistoryData(material));
  }, []);

  return (
    <Statistic
      data={fData ?? data}
      sortInfo={sortInfo}
      directionOfSorting={directionOfSorting}
      handleChange={handleChange}
      filterInfo={filterInfo}
    />
  );
}
