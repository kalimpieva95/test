import FilterListIcon from "@mui/icons-material/FilterList";
import FilterListOffIcon from "@mui/icons-material/FilterListOff";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Paper from "@mui/material/Paper";
import Select from "@mui/material/Select";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { CardModal } from "../Modal/Modal";
import "./Static.css";

const columns = [
  "country",
  "continent",
  "location",
  "confirmed",
  "recovered",
  "deaths",
  "people_partially_vaccinated",
  "people_vaccinated",
];

export default function Statistic({
  data,
  sortInfo,
  filterInfo,
  directionOfSorting,
  handleChange,
}) {
  const [country, setCountry] = useState("");

  if (!data) return null;

  return (
    <>
      <TableContainer component={Paper}>
        <Box sx={{ minWidth: 120 }}>
          <FormControl className="filterSelect">
            <InputLabel id="demo-simple-select-label">Filter</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="filter"
              onChange={handleChange}
            >
              {columns.map((item) => {
                return <MenuItem value={item}>{item}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Box>
        <TextField label="filter" variant="standard" onChange={filterInfo} />
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            {columns.map((title) => {
              return (
                <TableCell key={title} onClick={() => sortInfo(title)}>
                  {title}
                  {directionOfSorting ? (
                    <FilterListOffIcon />
                  ) : (
                    <FilterListIcon />
                  )}
                </TableCell>
              );
            })}
          </TableHead>
          <TableBody>
            {data?.map((data, index) => {
              return (
                <TableRow
                  key={index}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  onClick={() => setCountry(data.country)}
                >
                  {columns.map((key) => {
                    return (
                      <TableCell key={key} component="th" scope="row">
                        {data[key]}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <CardModal country={country} onClose={() => setCountry("")} />
    </>
  );
}
